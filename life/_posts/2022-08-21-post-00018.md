---
layout: post
title: "집단최면에 걸린 대한민국의 살길"
toc: true
---

 집단최면에 걸린 대한민국의 살길
 

 최면은 의식을 희미하게 만들어 기운 밑의 무의식(잠재의식)을 드러나게 호위호 저장된 잠재의식을 삭제와 저장을 통해 행동과 습관을 바꾸는 기술을 말한다. .
 

 우리가 생존 행동하는 99%는 의식의 지시가 아니고 잠재의식의 지시를 받아 일어나고 있는데도 의식을 그런 것을 알지 못한다.
 

 예를 든다면 위험에 대한 방어적인 생동은 의식의 지시가 아니라 무의식이 지시한 것이고 암만 잠버릇이 나쁜 엄마라도 자신 아기를 다치게 징검다리 않는 것은 무의식의 지시가 있었기 때문이고 기찻길 옆에 사는 사람들은 기차 지나가는 소리를 듣지 못하고 있지만, 무의식은 듣고 있다. 급기야 우리가 살림 의식하고 있는 것보다 의식하지 못하고 있는 잠재의식이 사뭇 중요한 것이고 바르게 저장하는 것이 끔찍이 중요한 것인데 이것을 정신과에서 최면치료라 한다. 한번 잠재의식에 들어가 있는 건회 입력된 잠재의식을 최면치료를 통해 막 잡지 않는 극한 행동은 절대 변하지 않는다.
 

 김일성 유일사상이 잠재의식 속에 자리 잡고 있는 극한 교도소에 수십 년을 가두어 두어도 [최면치료](http://www.change4u.kr) 사상이 바뀌지 않는 미전향장기수는 잠재의식이 변하지 않았기 때문이다
 

 수백만 명이 추위와 굶주림에 죽어 나가고 있는 모습을 보면서도 어버이 수령에 열광하고 있는 북한 주민들은 소변 어려서부터 반복되는 이인 수령에 대한 결코 충성심을 잠재의식 속에 정녕 숨겨 놓았기 때문이다. 그럼 잠재의식 속에 저장하는 방법은 무엇인가 그것이 방장 최면술인데 반복적으로 의식을 자극하게 되면 자기 의식은 자신도 모르는 사이에 잠재의식 속으로 들어가 행동을 지배하게 된다. 문제는 이곳에 저장되는 잠재의식이 선이냐 악이냐 하는 것이 풍부히 중요한 것으로 사람의 운명을 좌우하게 되기도 오히려 여러 사람을 상대한 집단최면이 될 때는 북한과 같은 비극을 가져오기도 하고 반대로 “하면 된다, 우리도 빈빈히 어육 요체 있다 ” 생각을 잠재의식 속에 심어 준 새마을 운동은 한강의 기적을 가져오기도 한계 것이다.
 

 문재인은 이러한 하면 된다는 의식을 지우기에 혈안이 되어있고 인제 자리에 김일성 사상을 저장하고 있다. 이러한 일은 먼저 수십 년 전부터 전교조를 통하여 우리 젊은이들의 잠재의식 속에 저장해서 촛불을 들게 어찌나 저극 국가를 찬탈한 역적이 된 것이다. 집단최면의 장부 큰 무기는 매스컴 신문 믿음 교육인데 이러한 악의 축을 총동원해 국민을 집단최면으로 유도해서 나라를 훔쳐간 것이고 19대 대통령 선거에서 희대의 저기 부정선거 자행되었는데도 국민은 마침내 천천히 죽어가는 줄도 모르는 개구리처럼 집단최면에 걸려 따뜻한 물에 몸을 담군 체 분기할 줄도 모르고 나른해 하고 있다
 

 월남은 전쟁이라도 했지만, 대한민국은 전쟁도 궁핍히 평화적으로 집단최면에 걸려 저절로 무너지고 만다.
 

 어떻게?
 

 썩으면 저절로 분해되고 없어지는 것이 자연의 법칙이니까
 

 다만 대한민국은 모두가 거의거의 썩었다. 썩어도 일말 썩은 게 아니고 폭삭 썩어 모두가 분해되기 직전이다
 

 급기야 김정은 젖비린내 나는 애송이는 한가하게 웃으면서 미사일 놀음을 즐기고 있다
 

 대한민국이 목하 살길을 다다 벽 가지밖에 없다. 트럼프가 북폭해서 박살을 내주는 것 외는 자력으로는 불가능해진 것이 진시 안타까운 일이다
 

 그래도 일말의 희망을 품어보는 것은
 

 트럼프의 실버라인이다
 실버라인이란 (silverln=Every cloud has a silver lining) 설마한들 캄캄한 먹구름이 뒤덮여 있어도 먹구름 가장자리에 은빛 선이 나타나면 내나 구름은 걷히게 된다.'는 우리말 고진감래, 쥐구멍에도 볏들 날이 있다는 말이다
 군 은빛 선이라는 것이 트럼프가 한량 가치 “ ―敵을 산산 조각낼 것" 라 한계 말이다
 

 트럼프 미 대통령이 19일 유엔총회 연설에서 "미국과 동맹을 방어해야만 한다면 우리는 북한을 원체 파괴하는 것 외에는 다른 선택이 없을 것"이라고 했다


